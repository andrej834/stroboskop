# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
rm README.md
rm hello-world.html
mkdir domaca
cd domaca
git clone https://andrej834@bitbucket.org/andrej834/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/andrej834/stroboskop/commits/ecc8cbdd287de7dcae570669942aa31e3cc49397

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/andrej834/stroboskop/commits/e2559cdb5b8c00a216842a5bdce5147479f16e0d

Naloga 6.3.2:
https://bitbucket.org/andrej834/stroboskop/commits/588040e73afdccc432444e31e491d6932bb42f79

Naloga 6.3.3:
https://bitbucket.org/andrej834/stroboskop/commits/cebf43bde711e9688975d9ca7eb02cee8f08908e

Naloga 6.3.4:
https://bitbucket.org/andrej834/stroboskop/commits/ca974e5621340912322ff76a3287cc361a64d3f9

Naloga 6.3.5:

```
git checkout master
git merge izgled 
git commit -m "Združeni veji izgled in master"
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/andrej834/stroboskop/commits/a337342ba998fcaf5e3b31de222734421c9d4515

Naloga 6.4.2:
https://bitbucket.org/andrej834/stroboskop/commits/cc469caa8ead0087ddd6067a70bd19fb7ab067b5

Naloga 6.4.3:
https://bitbucket.org/andrej834/stroboskop/commits/4801746409e48b9fae3f0b53c6bb918de2509930

Naloga 6.4.4:
https://bitbucket.org/andrej834/stroboskop/commits/6a50dc42b13adafdaab7112ad7cd8edd4059ac22